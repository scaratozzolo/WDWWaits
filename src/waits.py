import os
import datetime as dt
from datetime import datetime
import pathlib
from concurrent.futures import ThreadPoolExecutor
import configparser
import json
from collections import defaultdict
import requests
from pymongo import MongoClient
from scheduler import Scheduler
from loguru import logger

# logger.add("logs.log")

class Waits:

    def __init__(self):

        self.project_root = pathlib.Path(__file__).parent.parent

        self.config = configparser.ConfigParser()
        self.config.read(self.project_root / "src/config.ini")
        logger.info("Config loaded")

        self.mongo_client = MongoClient(self.config['MONGODB']['CONN_URI'])
        logger.info("Connected to MongoDB")
        db_name = "wdwwaits" if os.environ.get("DEBUG", False) else "wdwwaits_dev"
        logger.debug(f"{db_name=}")
        self.mongo_db = self.mongo_client[db_name]
        self.mongo_schedules_collection = self.mongo_db['schedules']
        self.mongo_weather_collection = self.mongo_db['weather']
        self.mongo_entities_collection = self.mongo_db['entities']
        self.mongo_wait_times_collection = self.mongo_db['wait_times'] # TODO this collection can hold current data so it can be queried easier

        # self.now = datetime.now()
        # self.today_str = self.now.strftime("%Y-%m-%d")
        # logger.debug(f"{self.now=}")

        self.themeparks_api_base = "https://api.themeparks.wiki/v1"

        self.wdw_dest_id = "e957da41-3552-4cf6-b636-5babc5cbc4e5"
        self.dlr_dest_id = "bfc89fd6-314d-44b4-b89e-df1a89cf991e"

        with open(self.project_root / "src/wdw_parks.json", "r") as wdw_file:
            self.wdw_parks = json.load(wdw_file)
            logger.debug("wdw_parks loaded")

        with open(self.project_root / "src/dlr_parks.json", "r") as dlr_file:
            self.dlr_parks = json.load(dlr_file)
            logger.debug("dlr_parks loaded")

    def get_weather(self):

        # TODO investigate 3.0 api call

        # TODO only run if data is more than x minutes old

        weather_key = self.config['SECRET_KEYS']['WEATHER_KEY']

        orlando_weather_resp = requests.get(f"https://api.openweathermap.org/data/2.5/weather?lat=28.388195&lon=-81.569324&units=imperial&appid={weather_key}").json()
        logger.debug("orlando weather pulled")
        anaheim_weather_resp = requests.get(f"https://api.openweathermap.org/data/2.5/weather?lat=33.808666&lon=-117.918955&units=imperial&appid={weather_key}").json()
        logger.debug("anaheim weather pulled")

        # TODO individual park weather

        data = {
            "date": self.now,
            self.wdw_dest_id : orlando_weather_resp,
            self.dlr_dest_id: anaheim_weather_resp,
        }

        mongo_weather_id = self.mongo_weather_collection.insert_one(data).inserted_id
        logger.info(f"Weather inserted, {mongo_weather_id=}")


    def get_schedules(self):

        if self.mongo_schedules_collection.find_one({"date": self.today_str}):
            logger.debug("today's schedule exists")
            if 'parks' in self.mongo_schedules_collection.find_one({"date": self.today_str}):
                logger.debug("parks schedule exists")
                return


        def get_schedule(id):

            park_schedules = []

            park_sched_resp = requests.get(f'{self.themeparks_api_base}/entity/{id}/schedule')

            if park_sched_resp.ok:
                logger.debug(f"schedule request ok: {id=}")
                data = park_sched_resp.json()
                for i in data['schedule']:
                    if i['date'] == self.today_str:
                        park_schedules.append(i)

            return park_schedules

        all_park_schedules = defaultdict(dict)

        for id, name in self.wdw_parks.items():
            all_park_schedules[id]['name'] = name
            all_park_schedules[id]['timezone'] = "America/New_York"
            all_park_schedules[id]['schedules'] = get_schedule(id)

        for id, name in self.dlr_parks.items():
            all_park_schedules[id]['name'] = name
            all_park_schedules[id]['timezone'] = "America/Los_Angeles"
            all_park_schedules[id]['schedules'] = get_schedule(id)

        data = {"date":self.today_str, "parks":all_park_schedules}

        result = self.mongo_schedules_collection.update_one({'date': self.today_str}, {'$set': data}, upsert=True)
        logger.info(f"Schedules inserted, {result.modified_count=}")

    
    def _get_destination_hours(self):
        # TODO

        self.mongo_schedules_collection.find_one({"date": self.today_str})
    
    def get_wait_times(self):
        
        wdw_children = requests.get(f'{self.themeparks_api_base}/entity/{self.wdw_dest_id}/children').json()
        logger.debug("wdw children pulled")
        dlr_children = requests.get(f'{self.themeparks_api_base}/entity/{self.dlr_dest_id}/children').json()
        logger.debug("dlr children pulled")

        with ThreadPoolExecutor() as executor:

            for i in wdw_children['children']:
                if i['entityType'] == "SHOW":
                    executor.submit(self._parse_entertainment_live_data, i['id'])
                    executor.submit(self._parse_entity_data, i['id'])
                elif i['entityType'] == "ATTRACTION":
                    executor.submit(self._parse_attraction_live_data, i['id'])
                    executor.submit(self._parse_entity_data, i['id'])

            for i in dlr_children['children']:
                if i['entityType'] == "SHOW":
                    executor.submit(self._parse_entertainment_live_data, i['id'])
                elif i['entityType'] == "ATTRACTION":
                    executor.submit(self._parse_attraction_live_data, i['id'])

                executor.submit(self._parse_entity_data, i['id'])

        logger.info("Finished wait times pull")

    def _parse_entity_data(self, id):

        entity_resp = requests.get(f'{self.themeparks_api_base}/entity/{id}').json()

        result = self.mongo_entities_collection.update_one({'id':id}, {'$set': entity_resp}, upsert=True)
        logger.debug(f"entities_collection updated, {result.modified_count=}, {id=}")
    
    # TODO most likely these two methods are going to be very identical, can probably just add some if statements
    def _parse_attraction_live_data(self, id):
        
        attract_resp = requests.get(f'{self.themeparks_api_base}/entity/{id}/live').json()

        live_data = attract_resp['liveData'][0]

        data = {'schedules': live_data['operatingHours'], 'name': live_data['name'], 'timezone': attract_resp['timezone']}

        result = self.mongo_schedules_collection.update_one({'date': self.today_str}, {'$set': {f'attractions.{id}':data}}, upsert=True)
        logger.debug(f"schedules_collection updated, {result.modified_count=}, {id=}")

        result = self.mongo_db[id].insert_one({'id':id, 'name': live_data['name'], 'entityType': 'ATTRACTION', 'status': live_data['status'], 'last_updated':live_data['lastUpdated'], 'date':self.now, 'queue': live_data['queue']})
        logger.debug(f"wait time data updated, {result.inserted_id=}, {id=}")

        result = self.mongo_wait_times_collection.update_one({'id':id}, {'$set': {'name': live_data['name'], 'entityType': 'ATTRACTION', 'status': live_data['status'], 'last_updated':live_data['lastUpdated'], 'date':self.now, 'queue': live_data['queue']}}, upsert=True)
        logger.debug(f"wait_times_collection updated, {result.modified_count=}, {id=}")

        # wdw tron 5a43d1a7-ad53-4d25-abfe-25625f0da304 for boarding group data
        # dlr space mountain 9167db1d-e5e7-46da-a07f-ae30a87bc4c4


    def _parse_entertainment_live_data(self, id):

        
        ent_resp = requests.get(f'{self.themeparks_api_base}/entity/{id}/live').json()

        live_data = ent_resp['liveData'][0]

        data = {'schedules': live_data['showtimes'], 'name': live_data['name'], 'timezone': ent_resp['timezone']}

        result = self.mongo_schedules_collection.update_one({'date': self.today_str}, {'$set': {f'entertainments.{id}':data}}, upsert=True)
        logger.debug(f"schedules_collection updated, {result.modified_count=}, {id=}")

        result = self.mongo_db[id].insert_one({'id':id, 'name': live_data['name'], 'entityType': 'ENTERTAINMENT', 'status': live_data['status'], 'last_updated':live_data['lastUpdated'], 'date':self.now})
        logger.debug(f"wait time data updated, {result.inserted_id=}, {id=}")

        if 'queue' in live_data:
            queue_result = self.mongo_db[id].update_one({'_id':result.inserted_id}, {'$set': {'queue': live_data['queue']}})
            logger.debug(f"queue data added, {queue_result.modified_count=}, {id=}")

            result = self.mongo_wait_times_collection.update_one({'id':id}, {'$set': {'name': live_data['name'], 'entityType': 'ENTERTAINMENT', 'status': live_data['status'], 'last_updated':live_data['lastUpdated'], 'date':self.now, 'queue': live_data['queue']}}, upsert=True)
            logger.debug(f"wait_times_collection updated, {result.modified_count=}, {id=}")
        # TODO add queue information
        # 012a211b-4c91-451c-8a0e-5e3ab398eda8
        # 7e621af5-8a0e-4427-912e-f49b438656da


    def main(self):

        logger.info(f"{len(schedule.jobs)=}")

        self.now = datetime.now()
        self.today_str = self.now.strftime("%Y-%m-%d")
        logger.debug(f"{self.now=}")

        self.get_schedules()

        self.get_weather()

        self.get_wait_times()


if __name__ == '__main__':

    waits_obj = Waits()
    schedule = Scheduler()

    # for i in range(10, 70, 10):
    #     schedule.once(dt.timedelta(minutes=i), waits_obj.main)

    schedule.cyclic(dt.timedelta(minutes=10), waits_obj.main)
    import time
    # while len(schedule.jobs):
    while True:
        schedule.exec_jobs()
        time.sleep(1)

